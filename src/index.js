import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

function Casilla(props){

        let elemento = <button className="square" onClick={props.onClick}>
            {props.value}
        </button>
        return(elemento);
}

class Grilla extends React.Component{

    definirCasilla(indice){
        let elemento = 
            <Casilla 
            key={indice} value={this.props.squares[indice]}
            onClick={() => this.props.onClick(indice)}/> 
        return (elemento);
    }
    render(){

        let miGrilla = [];

        for(let i=0; i<3; i++){
            let casillas=[]
            for(let k=0; k<3; k++){
                casillas.push(this.definirCasilla(i*3+k));
            }
            miGrilla.push(<div className="fila">{casillas}</div>)    
        }
        return(miGrilla);
    }
}

class Juego extends React.Component{

    constructor(props){
        super (props);   //Se debe llamar siempre al constructor de la clase padre.
        this.state = {  // Estado
            history: [
                {
                    squares: [
                        ["casilla1", "casilla2", "casilla3"],
                        ["casilla4", "casilla5", "casilla6"],
                        ["casilla7", "casilla8", "casilla9"]
                    ].fill(null)
                }
            ],
            stepNumber: 0,
            xIsNext: true   
        };
    }
    click(i){
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length -1];
        const squares = current.squares.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
          }
          squares[i] = this.state.xIsNext ? "X" : "O";
          this.setState({
            history: history.concat([
              {
                squares: squares
              }
            ]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext
          });
        }

        jumpTo(step) {
            this.setState({
              stepNumber: step,
              xIsNext: (step % 2) === 0
            });
          }
        
        render() {
            const history = this.state.history;
            const current = history[this.state.stepNumber];
            const winner = calculateWinner(current.squares);
        
            const moves = history.map((step, move) => {
              const desc = move ?
                'Go to move #' + move :
                'Go to game start';
              return (
                <li key={move}>
                  <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
              );
            });
        
            let status;
            if (winner) {
              status = "Winner: " + winner;
            } else {
              status = "Next player: " + (this.state.xIsNext ? "X" : "O");
            }
        
            return (
              <div className="game">
                <div className="game-board">
                  <Grilla
                    squares={current.squares}
                    onClick={i => this.click(i)}
                  />
                </div>
                <div className="game-info">
                  <div>{status}</div>
                  <ol>{moves}</ol>
                </div>
              </div>
            );
    }
        
}

ReactDOM.render(<Juego/>, document.getElementById('root'))

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return squares[a];
      }
    }
    return null;
  }